package com.example.tarea_lab_00

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener {

            tvResultado.text= when (edtEdad.text.toString().toInt()) {
                in 1..17 -> {
                    "Usted es menor de edad"
                }
                in 18..105 -> {
                    "Usted es mayor de edad"
                }
                else -> {
                    "¿Seguro esta vivo?"
                }
            }
        }

    }
}